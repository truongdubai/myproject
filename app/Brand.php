<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
    protected $fillable = ['name', 'manager', 'year'];

    public static function index()
    {
    	if (request()->ajax()) {
            return datatables()->of(Brand::latest()->get())
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="edit" id="' . $data->id . '" class="edit btn btn-primary btn-sm">Edit</button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm">Delete</button>';
                    return $button;

                })
                ->rawColumns(['action'])
                ->make(true);
        }
    	return view('brands');
    }
    public static function store($request)
    {
    	$form_data = array(
    		'name'    => $request->name,
    		'manager' => $request->manager,
    		'year'    => $request->year,
    	);
    	Brand::create($form_data);
    	return response()->json(['success' => 'Thêm thành công.']);
    }
    public static function edit($id)
    {
        if (request()->ajax()){
            $data = Brand::findOrFail($id);
            return response()->json(['data' => $data]);
        }
    }
    public static function updated($request)
    {
        $form_data = array(
            'name'      => $request->name,
            'manager'   => $request->manager,
            'year'      => $request->year,
        );
        Brand::whereId($request->hidden_id)->update($form_data);
        return response()->json(['success' => "Cập nhật thành công."]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Http\Requests\StoreBrandRequest;
use App\Http\Requests\UpdateRequest;
class BrandController extends Controller
{
    public function index()
    {
    	return Brand::index();
    }
    public function store(StoreBrandRequest $request)
    {
    	return Brand::store($request);
    }
    public function edit($id)
    {
    	return Brand::edit($id);
    }
    public function update(UpdateRequest $request)
    {
        return Brand::updated($request);
    }
}

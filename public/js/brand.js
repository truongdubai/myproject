$(document).ready(function(){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$('#user_table').DataTable({
		processing: true,
		serverSide: true,
		ajax:{
			url: 'brands',
		},
		columns:[
		{
			data: 'name',
			name: 'name'
		},
		{
			data: 'manager',
			name: 'manager'
		},
		{
			data: 'year',
			name: 'year'
		},
		{
			data: 'action',
			name: 'action',
			orderable: false
		}]
	});

	$('#create_record').click(function(){
		$('.modal-title').text("Them Moi");
			$('#action_button').val("Add");
			$('#action').val("Add");
			$('#formModal').modal('show');
	});

	$('#sample_form').on('submit', function(event){
		var id = $(this).attr('id');
		event.preventDefault();
		if($('#action').val() == 'Add')
		{
			$.ajax({
				url:"brands/",
				method:"POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false,
				dataType: "json",
				success: function(data)
				{
					console.log(data);
					var html = '';
					if(data.errors)
					{
						html = '<div class="alert alert-danger">';
						for(var count = 0; count < data.errors.length; count++)
						{
							html += '<p>' + data.errors[count] + '</p>';
						}
						html += '</div>';
					}
					if(data.success)
					{
						html = '<div class="alert alert-success">' + data.success + '</div>';
						$('#sample_form')[0].reset();
						$('#user_table').DataTable().ajax.reload();
					}
					$('#form_result').html(html);
					$('#formModal').modal('hide');
				}
			})
		}
		if($('#action').val() == "Edit")
		{
			$.ajax({
				url:"brands/" + id,
				method:"PUT",
				data: $('#sample_form').serialize(),
				cache: false,
				processData: false,
				dataType:"json",
				success:function(data)
				{
					var html = '';
					if(data.errors)
					{
						html = '<div class="alert alert-danger">';
						for(var count = 0; count < data.errors.length; count++)
						{
							html += '<p>' + data.errors[count] + '</p>';
						}
						html += '</div>';
					}
					if(data.success)
					{
						html = '<div class="alert alert-success">' + data.success + '</div>';
						$('#sample_form')[0].reset();
						$('#store_image').html('');
						$('#user_table').DataTable().ajax.reload();
					}
					$('#form_result').html(html);
					$('#formModal').modal('hide');
				}
			})
		}
	});

	$(document).on('click', '.edit', function(){
		var id = $(this).attr('id');
		$('#form_result').html('');
		$.ajax({
			url:"/brands/"+id+"/edit",
			dataType:"json",
			success:function(html){
				$('#club').val(html.data.club);
				$('#coach').val(html.data.coach);
				$('#stadium').val(html.data.stadium);
				$('#hidden_id').val(html.data.id);
				$('.modal-title').text("Edit");
				$('#action_button').val("Edit");
				$('#action').val("Edit");
				$('#formModal').modal('show');
			}
		})
	});
});
<html>
<head>
	<meta charset="UTF-8">
	<title>Brands</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
	<div class="container">
		<h3 align="center">Movie Entertainment</h3>
		<br />
		<div align="left">
			<button type="button" name="create_record" id="create_record" class="btn btn-success btn-sm">Thêm Mới</button>
		</div>
		<br />
		<div class="table-responsive">
			<table class="table table-bordered table-striped" id="user_table">
				<thead>
					<tr>
						<th width="25%">Hãng Phim</th>
						<th width="25%">Quản Lý</th>
						<th width="25%">Năm Thành Lập</th>
						<th width="25%">Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	@include('modal.AddModal')
</body>
	<script src="{{url('js/brand.js')}}"></script>
</html>